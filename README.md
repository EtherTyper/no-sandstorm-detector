# No Sandstorm Detector

To use this project, please create a `config.json` file of the following format:

```json
{
  "key": "<Blue Alliance API key>",
  "competitionKey": "<competition key>"
}
```

To get a Blue Alliance read API key, go to [their website](https://www.thebluealliance.com/account).

To get the key for an event, find its page on Blue Alliance and extract it from the URL.
For example, in `https://www.thebluealliance.com/event/2019txaus` the key would be `2019txaus`.