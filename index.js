(async function () {

  const { key, competitionKey } = require("./config")
  const fetch = require("node-fetch")

  let blueAlliance = "https://www.thebluealliance.com/api/v3"
  let headers = {
    "Content-Type": "application/json",
    "X-TBA-Auth-Key": key
  }

  let statusResponse = await fetch(`${blueAlliance}/status`, { headers })
  let status = await statusResponse.json()

  let competitionResponse = await fetch(`${blueAlliance}/event/${competitionKey}/teams/statuses`, { headers })
  let competitionTeams = await competitionResponse.json()

  let teamNumbers = Object.keys(competitionTeams).sort((team1, team2) => {
    let number1 = parseInt(team1.substring(3))
    let number2 = parseInt(team2.substring(3))

    return number1 - number2
  })

  for (let team of teamNumbers) {
    let lastMatch = competitionTeams[team].last_match_key

    if (lastMatch !== null) {
      let matchReponse = await fetch(`${blueAlliance}/match/${lastMatch}`, { headers })
      let match = await matchReponse.json()
      let matchNumber = match.match_number

      let alliance = match.alliances.blue.team_keys.includes(team) ? "blue" : "red"
      let robotPosition = match.alliances[alliance].team_keys.indexOf(team) + 1
      let habLineStatus = match.score_breakdown[alliance][`habLineRobot${robotPosition}`]

      if (habLineStatus !== "CrossedHabLineInSandstorm") {
        console.log(`${team} in match ${matchNumber}: ${habLineStatus} only!`)
      }
    } else {
      console.log(`${team} has not had any matches yet.`)
    }
  }

})()
